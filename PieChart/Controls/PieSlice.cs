﻿namespace PieChart
{
    public class PieSlice : IPieSlice
    {
        public double Value { get; set; }

        public string Name { get; set; }

        public string ToolTip { get; set; }

        public PieSlice(string name, double val,string tip)
        {
            Name = name;
            Value = val;
            ToolTip = tip;
        }
     
        public PieSlice(string name, double val)
        {
            Name = name;
            Value = val;
        }
        public PieSlice(double val)
        {
            Value = val;
        }
        public PieSlice()
        {

        }
    }
}
