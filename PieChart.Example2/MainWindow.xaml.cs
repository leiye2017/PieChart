﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PieChart.Example2
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            double[] values = new double[8];
            Random r = new Random();
            for (int i = 0; i < 8; i++)
			{
                values[i] = r.Next(20, 50);
            }


            PieSlice[] slices = new PieSlice[6];
            slices[0] = new PieSlice("A:",30,"这是测试");
            slices[1] = new PieSlice("B:", 10, "仅能看饼图");
            slices[2] = new PieSlice("C:", 40, " 无文字查 ");
            slices[3] = new PieSlice("D:", 20, "效果图  效果图 ");
            slices[4] = new PieSlice("E:", 35, "利用矩阵做了许多运算");
            slices[5] = new PieSlice("F:", 39, "再将其平移回来即");
  
            pie1.ItemsSource = slices;

        }
    }
}
